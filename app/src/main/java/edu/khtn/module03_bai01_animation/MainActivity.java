package edu.khtn.module03_bai01_animation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity
        implements View.OnClickListener {
    Button buttonStart;
    EditText editCount, editDuration;
    EditText editAlphaO, editAlphaI, editRotateR, editRotateL,
            editTransL, editTransR, editTransU, editTransD,
            editScaleI, editScaleO;
    RadioButton radioRestart, radioReverse;
    RadioGroup interpolatorRadio, startGroup;
    CheckBox checkAlphaO, checkAlphaI, checkRotateR, checkRotateL,
            checkTransL, checkTransR, checkTransU, checkTransD,
            checkScaleI, checkScaleO;
    String sCount, sDuration;
    String sAlphaO, sAlphaI, sRotateR, sRotateL,
            sTransL, sTransR, sTransU, sTransD,
            sScaleI, sScaleO;
    android.view.animation.Interpolator
            bounce, celerate, anticipate, accelerate, cycle, decelerate,
            linear, overshoot,fastOutLineIn,fastOutSlowIn,lineOutSlowIn;
    List<Interpolator> interpolatorList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linkAllViewAndSetListener();
        createInterpolator();
        editDuration.setText(1000 + "");
        editCount.setText(1+"");
        setupAnimation(checkAlphaI, R.animator.alpha_i);
        setupAnimation(checkAlphaO, R.animator.alpha_o);
        setupAnimation(checkTransL, R.animator.trans_l);
        setupAnimation(checkTransR, R.animator.trans_r);
        setupAnimation(checkTransU, R.animator.trans_u);
        setupAnimation(checkTransD, R.animator.trans_d);
        setupAnimation(checkScaleI, R.animator.scale_i);
        setupAnimation(checkScaleO, R.animator.scale_o);
        setupAnimation(checkRotateL, R.animator.rotate_l);
        setupAnimation(checkRotateR, R.animator.rotate_r);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonStart) {
            start(createInterpolator());
        }
    }

    public void setupAnimation(final View view, final int animationID) {
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Animator animator = AnimatorInflater.
                        loadAnimator(MainActivity.this, animationID);
                animator.setTarget(view);
                animator.start();
            }
        });
    }

    public Interpolator createInterpolator(){
        switch (interpolatorRadio.getCheckedRadioButtonId()){
            case R.id.radio_bounce:
                return bounce = new BounceInterpolator();
            case R.id.radio_celerate:
                return celerate = new AccelerateDecelerateInterpolator();
            case R.id.radio_accelerate:
                return accelerate = new AccelerateInterpolator();
            case R.id.radio_Decelerate:
                return decelerate = new DecelerateInterpolator();
            case R.id.radio_anticipate:
                return anticipate = new AnticipateInterpolator();
            case R.id.radio_cycle:
                return cycle = new CycleInterpolator(1);
            case R.id.radio_linear:
                return linear = new LinearInterpolator();
            case R.id.radio_overshoot:
                return overshoot = new OvershootInterpolator();
            case R.id.radio_fastOutLineIn:
                return fastOutLineIn = new FastOutLinearInInterpolator();
            case R.id.radio_fastOutSlowIn:
                return fastOutSlowIn = new FastOutSlowInInterpolator();
            case R.id.radio_lineOutSlowIn:
                return lineOutSlowIn = new LinearOutSlowInInterpolator();
            default:
                break;
        }
        return null;
    }

    public void start(android.view.animation.Interpolator interpolator) {
        sCount = editCount.getText().toString();
        sDuration = editDuration.getText().toString();
        sAlphaI = editAlphaI.getText().toString();
        sAlphaO = editAlphaO.getText().toString();
        sRotateL = editRotateL.getText().toString();
        sRotateR = editRotateR.getText().toString();
        sScaleI = editScaleI.getText().toString();
        sScaleO = editScaleO.getText().toString();
        sTransL = editTransL.getText().toString();
        sTransU = editTransU.getText().toString();
        sTransD = editTransD.getText().toString();
        sTransR = editTransR.getText().toString();
        AnimationSet animSet = new AnimationSet(false);
        if (sDuration.isEmpty()) {
            toast("input Duration");
        } else {
            if (checkAlphaI.isChecked()) {
                AlphaAnimation alphaI = (AlphaAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.alpha_i);
                alphaI.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    alphaI.setRepeatCount(Integer.parseInt(sCount));
                if (!sAlphaI.isEmpty())
                    alphaI.setStartOffset(Long.parseLong(sAlphaI));
                animSet.addAnimation(alphaI);
            }
            if (checkAlphaO.isChecked()) {
                AlphaAnimation alphaO = (AlphaAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.alpha_o);
                alphaO.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    alphaO.setRepeatCount(Integer.parseInt(sCount));
                if (!sAlphaO.isEmpty())
                    alphaO.setStartOffset(Long.parseLong(sAlphaO));
                animSet.addAnimation(alphaO);
            }
            if (checkRotateL.isChecked()) {
                RotateAnimation rotateL = (RotateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.rotate_l);
                rotateL.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    rotateL.setRepeatCount(Integer.parseInt(sCount));
                if (!sRotateL.isEmpty())
                    rotateL.setStartOffset(Long.parseLong(sRotateL));
                animSet.addAnimation(rotateL);
            }
            if (checkRotateR.isChecked()) {
                RotateAnimation rotateR = (RotateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.rotate_r);
                rotateR.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    rotateR.setRepeatCount(Integer.parseInt(sCount));
                if (!sRotateR.isEmpty())
                    rotateR.setStartOffset(Long.parseLong(sRotateR));
                animSet.addAnimation(rotateR);
            }
            if (checkScaleI.isChecked()) {
                ScaleAnimation scaleI = (ScaleAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.scale_l);
                scaleI.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    scaleI.setRepeatCount(Integer.parseInt(sCount));
                if (!sScaleI.isEmpty())
                    scaleI.setStartOffset(Long.parseLong(sScaleI));
                animSet.addAnimation(scaleI);
            }
            if (checkScaleO.isChecked()) {
                ScaleAnimation scaleO = (ScaleAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.scale_o);
                scaleO.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    scaleO.setRepeatCount(Integer.parseInt(sCount));
                if (!sScaleO.isEmpty())
                    scaleO.setStartOffset(Long.parseLong(sScaleO));
                animSet.addAnimation(scaleO);
            }
            if (checkTransL.isChecked()) {
                TranslateAnimation transL = (TranslateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.trans_l);
                transL.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    transL.setRepeatCount(Integer.parseInt(sCount));
                if (!sTransL.isEmpty())
                    transL.setStartOffset(Long.parseLong(sTransL));
                animSet.addAnimation(transL);
            }
            if (checkTransR.isChecked()) {
                TranslateAnimation transR = (TranslateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.trans_r);
                transR.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    transR.setRepeatCount(Integer.parseInt(sCount));
                if (!sTransR.isEmpty())
                    transR.setStartOffset(Long.parseLong(sTransR));
                animSet.addAnimation(transR);
            }
            if (checkTransU.isChecked()) {
                TranslateAnimation transU = (TranslateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.trans_u);
                transU.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    transU.setRepeatCount(Integer.parseInt(sCount));
                if (!sTransU.isEmpty())
                    transU.setStartOffset(Long.parseLong(sTransU));
                animSet.addAnimation(transU);
            }
            if (checkTransD.isChecked()) {
                TranslateAnimation transD = (TranslateAnimation) AnimationUtils.
                        loadAnimation(MainActivity.this, R.anim.trans_d);
                transD.setInterpolator(interpolator);
                if (!sCount.isEmpty())
                    transD.setRepeatCount(Integer.parseInt(sCount));
                if (!sTransD.isEmpty())
                    transD.setStartOffset(Long.parseLong(sTransD));
                animSet.addAnimation(transD);
            }
            if (radioRestart.isChecked()) {
                animSet.setRepeatMode(Animation.RESTART);
            } else {
                animSet.setRepeatMode(Animation.REVERSE);
            }
            animSet.setDuration(Long.parseLong(sDuration));
            buttonStart.startAnimation(animSet);
        }
    }

    private void linkAllViewAndSetListener() {
        buttonStart = (Button) findViewById(R.id.button_start);
        buttonStart.setOnClickListener(this);
        editCount = (EditText) findViewById(R.id.edit_count);
        editDuration = (EditText) findViewById(R.id.edit_duration);
        editAlphaO = (EditText) findViewById(R.id.edit_alphaO);
        editAlphaI = (EditText) findViewById(R.id.edit_alphaI);
        editRotateL = (EditText) findViewById(R.id.edit_rotateL);
        editRotateR = (EditText) findViewById(R.id.edit_rotateR);
        editTransL = (EditText) findViewById(R.id.edit_transL);
        editTransR = (EditText) findViewById(R.id.edit_transR);
        editTransU = (EditText) findViewById(R.id.edit_transU);
        editTransD = (EditText) findViewById(R.id.edit_transD);
        editScaleI = (EditText) findViewById(R.id.edit_scaleI);
        editScaleO = (EditText) findViewById(R.id.edit_scaleO);
        radioRestart = (RadioButton) findViewById(R.id.radio_restart);
        radioReverse = (RadioButton) findViewById(R.id.radio_reverse);
        interpolatorRadio = (RadioGroup) findViewById(R.id.interpolator_group);
        startGroup = (RadioGroup) findViewById(R.id.start_group);
        checkAlphaI = (CheckBox) findViewById(R.id.check_alphaI);
        checkAlphaO = (CheckBox) findViewById(R.id.check_alphaO);
        checkTransL = (CheckBox) findViewById(R.id.check_transL);
        checkTransR = (CheckBox) findViewById(R.id.check_transR);
        checkTransU = (CheckBox) findViewById(R.id.check_transU);
        checkTransD = (CheckBox) findViewById(R.id.check_transD);
        checkScaleI = (CheckBox) findViewById(R.id.check_scaleI);
        checkScaleO = (CheckBox) findViewById(R.id.check_scaleO);
        checkRotateL = (CheckBox) findViewById(R.id.check_rotateL);
        checkRotateR = (CheckBox) findViewById(R.id.check_rotateR);
    }

    public void toast(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
